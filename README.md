# ROMEO: Exploring Juliet through the Lens of Assembly Language

## What is it?
The ROMEO dataset serves as a machine code counterpart to Juliet, to verify vulnerability detection methods that operate on binary or assembly representations. We also offer an assembly language representation in this repository. Our focus is on the x86-64 ISA.

### Juliet?
The Juliet Test Suite for C/C++ 1.3 ([Juliet](https://samate.nist.gov/SRD/around.php#juliet_documents)) is a part of the Software Assurance Reference Database ([SARD](https://samate.nist.gov/SRD/index.php)) contributed by the NSA Center for Assured Software. It consists of 64,099 test cases covering 118 different Common Weakness Enumerations (CWEs) in a variety of situations.

## Publication
This dataset is provided as part of a [Computers & Security publication](https://doi.org/10.1016/j.cose.2023.103165) ([preprint](https://arxiv.org/abs/2112.06623)). A citation is always appreciated:
```bibtex
@Article{Brust2023ROMEO,
    author = {Clemens-Alexander Brust and Tim Sonnekalb and Bernd Gruner},
    title = {{ROMEO}: A binary vulnerability detection dataset for exploring Juliet through the lens of assembly language},
    year = {2023},
    journal = {Computers & Security},
    volume = {128},
    pages = {103165},
    issn = {0167-4048},
    doi = {https://doi.org/10.1016/j.cose.2023.103165}
}
```

## Raw Data
The binaries are provided as 66,147 object files in ELF format as well as linked-together files as specified in the paper. They are compiled using GCC 11.2.0 targeting x86_64-pc-linux-gnu. You can find them in the `object_files` directory. Alternatively, you can build your own by downloading Juliet 1.3 and executing `create_per_cwe_files.py` and then `make`.

## Reproducing the Results
In the `pipelines` folder, we supply configurations to build the full dataset as well as the no-context variant. For example, to build the full ROMEO, change to the `python` directory and execute:
```bash
python -m run_pipeline ../pipelines/fulldataset.yaml fulldataset
```

This pipeline requires you to set the `JULIET_BASE_DIR` environment variable to the location of the `C` folder of the Juliet suite if you wish to use your own distribution.
If you do not set this variable, it will extract the included object files to `~/.local/share/romeo` automatically.
It will also train and evaluate a Transformer-based model after the dataset has been created. You can find the results in the `intermediate` directory.

Executing might take a few hours, but if you simply wish to verify whether everything works, you can append `--smoke-test` to the command line for a fast run-through of the pipeline.

### Prerequisites
ROMEO requires the packages specified in `requirements.txt` along with Python 3.9. In addition, a current version of GNU binutils is required to disassemble the object files. For our paper, we used `objdump` version `2.27-44`. If you wish to train the Transformer model, a GPU is highly recommended.
