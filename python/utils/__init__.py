"""
    utils: Utility functions that have no other place to live.
"""

import typing


def _count_testcases_per_cwe(cwes: list[dict], testcases: list[dict]):
    """Returns the number of testcases associated with each CWE."""
    testcase_count = dict()
    for cwe in sorted(cwes, key=lambda c: c["WeaknessID"]):
        testcase_count[cwe["WeaknessID"]] = len(
            [
                testcase
                for testcase in testcases
                if testcase["Weakness"]["WeaknessID"] == cwe["WeaknessID"]
            ]
        )

    return testcase_count


def print_testcases_per_cwe(
    cwes: list[dict], testcases: list[dict], comment: typing.Optional[str] = None
) -> None:
    """Prints a table of CWEs with the corresponding number of testcases."""
    total = 0

    testcase_count = _count_testcases_per_cwe(cwes, testcases)

    print(
        "| CWE | Short Description                                            | Count    |"
    )
    print(
        "|-----|--------------------------------------------------------------|----------|"
    )
    for cwe in sorted(cwes, key=lambda c: c["WeaknessID"]):
        print(
            f'| {cwe["WeaknessID"]:3d} | {cwe["WeaknessShortenedName"]:60.60s} | {testcase_count[cwe["WeaknessID"]]:8d} |'
        )
        total += testcase_count[cwe["WeaknessID"]]

    print()
    print("Descriptive statistics:")
    print(
        f"Testcases: {total}, CWEs: {len(cwes)}"
        if comment is None
        else f"Testcases ({comment}): {total}, CWEs ({comment}): {len(cwes)}"
    )


__all__ = ["print_testcases_per_cwe"]
