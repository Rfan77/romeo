import itertools
import pathlib

import juliet_access
import numpy as np
import yaml

RUNS = ["fulldataset", "fulldataset-nocontext"]
SPLITS = ["Training", "Validation", "Test"]
REPETITIONS = ["1", "2", "3"]
COMBINATIONS = list(itertools.product(RUNS, SPLITS))


def merge_dicts(dicts):
    """Merge dictionaries with identical keys by computing the mean of the values."""
    result = {}
    for key in dicts[0].keys():
        all_values = [dict[key] for dict in dicts if dict[key] is not None]
        if len(all_values) == 0:
            result[key] = None
            continue

        if all([isinstance(value, dict) for value in all_values]):
            result[key] = merge_dicts(all_values)
        elif all([isinstance(value, float) for value in all_values]):
            result[key] = np.mean(all_values)
            result[f"_std_{key}"] = np.std(all_values)
        elif all([isinstance(value, int) for value in all_values]):
            result[key] = int(np.round(np.mean(all_values)))
        else:
            raise ValueError(f"Unsupported type: {all_values}")

    return result


def main(run_base_dir_parent: pathlib.Path):
    # Load eval results for all runs and splits and repetitions
    results = dict()
    for run in RUNS:
        results[run] = dict()
        for split in SPLITS:
            rep_results = []
            for repetition in REPETITIONS:
                eval_filename = (
                    run_base_dir_parent
                    / f"run-{run}-{repetition}/evaluation_{split}.yaml"
                )
                with open(eval_filename) as eval_file:
                    rep_results += [yaml.load(eval_file, yaml.Loader)]

            # Merge rep_results
            results[run][split] = merge_dicts(rep_results)

    # Collect flow variants
    flow_variants = {
        v["FlowVariantID"]: v for v in juliet_access.enumerate_flow_variants()
    }

    # Table (1): CWEs and # of examples
    cwes = juliet_access.enumerate_cwes()
    print("---")
    table_rows = []
    header = ["CWE ID", "Description"] + [
        f"{run}.{split}" for run, split in COMBINATIONS
    ]

    for cwe in cwes:
        cwe_id = cwe["WeaknessID"]
        cwe_description = cwe["WeaknessShortenedName"]
        if (
            cwe_id
            not in results[COMBINATIONS[0][0]][COMBINATIONS[0][1]]["PerCWE"].keys()
        ):
            continue
        table_rows += [
            [cwe_id, cwe_description]
            + [
                results[run][split]["PerCWE"][cwe_id]["TotalExamples"]
                for run, split in COMBINATIONS
            ]
        ]

    # Sort by number of examples
    table_rows = list(sorted(table_rows, key=lambda row: row[2], reverse=True))
    table_rows = [header] + table_rows
    table_rows += [
        ["Total", ""]
        + [
            results[run][split]["Overall"]["TotalExamples"]
            for run, split in COMBINATIONS
        ]
    ]
    for table_row in table_rows:
        print(" & ".join([str(col) for col in table_row]) + " \\\\")

    # Table (2) Accuracy with and without context per CWE
    print("---BYCWE")
    table_rows = []
    header = (
        ["CWE ID", "Description"]
        + [f"{run}.Accuracy" for run in RUNS]
        + [f"{run}.F1" for run in RUNS]
    )

    for cwe in cwes:
        cwe_id = cwe["WeaknessID"]
        cwe_description = cwe["WeaknessShortenedName"]
        if (
            cwe_id
            not in results[COMBINATIONS[0][0]][COMBINATIONS[0][1]]["PerCWE"].keys()
        ):
            continue

        table_rows += [
            [cwe_id, cwe_description]
            + [results[run]["Test"]["PerCWE"][cwe_id]["Accuracy"] for run in RUNS]
            + [results[run]["Test"]["PerCWE"][cwe_id]["TotalExamples"] for run in RUNS]
        ]  # + [results[run]["Test"]["PerCWE"][cwe_id]["F1"] for run in RUNS]]

    # Sort by number of examples
    table_rows = list(
        sorted(
            table_rows,
            key=lambda row: (row[2] - row[3]) * row[4]
            if (row[2] is not None and row[3] is not None and row[4] is not None)
            else 0.0,
            reverse=True,
        )
    )
    table_rows = [header] + table_rows
    table_rows += [
        ["Overall/Total", ""]
        + [results[run]["Test"]["Overall"]["Accuracy"] for run in RUNS]
        + [results[run]["Test"]["Overall"]["TotalExamples"] for run in RUNS]
    ]  # + [results[run]["Test"]["Overall"]["F1"] for run in RUNS]]
    for table_row in table_rows:
        for c, col in enumerate(table_row):
            if isinstance(col, float):
                table_row[c] = f"{col*100.0:02.1f}"
    for table_row in table_rows:
        print(" & ".join([str(col) for col in table_row]) + " \\\\")

    # Table (3) Accuracy with and without context per Flow Variant
    print("---BYFLOWVARIANT")
    table_rows = []
    header = (
        ["Flow Variant", "Description"]
        + [f"{run}.Accuracy" for run in RUNS]
        + [f"{run}.F1" for run in RUNS]
    )

    for flow_variant in flow_variants.keys():
        if (
            flow_variant
            not in results[COMBINATIONS[0][0]][COMBINATIONS[0][1]][
                "PerFlowVariant"
            ].keys()
        ):
            continue

        table_rows += [
            [flow_variant, flow_variants[flow_variant]["Description"]]
            + [
                results[run]["Test"]["PerFlowVariant"][flow_variant]["Accuracy"]
                for run in RUNS
            ]
            + [
                results[run]["Test"]["PerFlowVariant"][flow_variant]["TotalExamples"]
                for run in RUNS
            ]
        ]  # + [results[run]["Test"]["PerCWE"][cwe_id]["F1"] for run in RUNS]]

    # Sort by number of examples
    table_rows = list(
        sorted(
            table_rows,
            key=lambda row: (row[2] - row[3]) * row[4]
            if (row[2] is not None and row[3] is not None and row[4] is not None)
            else 0.0,
            reverse=True,
        )
    )
    table_rows = [header] + table_rows
    table_rows += [
        ["Overall/Total", ""]
        + [results[run]["Test"]["Overall"]["Accuracy"] for run in RUNS]
        + [results[run]["Test"]["Overall"]["TotalExamples"] for run in RUNS]
    ]  # + [results[run]["Test"]["Overall"]["F1"] for run in RUNS]]
    for table_row in table_rows:
        for c, col in enumerate(table_row):
            if isinstance(col, float):
                table_row[c] = f"{col*100.0:02.1f}"
    for table_row in table_rows:
        print(" & ".join([str(col) for col in table_row]) + " \\\\")

    # Table (4) Values with and without context overall
    print("---OVERALL")
    for run in RUNS:
        print(f"{run}")
        for key, value in results[run]["Test"]["Overall"].items():
            print(f"  {key}: {value}")

    # Table (5) Values with and without context overall for BVDetector
    print("---BVDETECTOR")
    for run in RUNS:
        print(f"{run}")
        for subset in results[run]["Test"]["BVDetector"].keys():
            print(f"  {subset}")
            for key, value in results[run]["Test"]["BVDetector"][subset].items():
                print(f"    {key}: {value}")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "run_base_dir_parent", type=pathlib.Path, help="Intermediate directory"
    )
    args = parser.parse_args()
    main(args.run_base_dir_parent)
