"""
    pipeline_steps: Actual implementations of the steps in the ROMEO pipeline.
"""

from .evaluation_pipeline_steps import (
    EvaluatePredictionsPipelineStep,
    PrintEvalulationTablePipelineStep,
)
from .juliet_pipeline_steps import (
    EnumerateCWEsPipelineStep,
    EnumerateTestcasesPipelineStep,
    FilterCWEsPipelineStep,
    FilterTestcasesPipelineStep,
    PrintTestcasesPerCWEPipelineStep,
)
from .preprocessing_pipeline_steps import (
    AssignSplitToExamplesPipelineStep,
    ExtractExamplesPipelineStep,
    LabelExamplesPipelineStep,
    RemoveDuplicateExamplesPipelineStep,
    ValidateTestcaseFilesPipelineStep,
)
from .transformer_pipeline_steps import (
    PredictSplitPipelineStep,
    TokenizeExamplesPipelineStep,
    TrainClassifierPipelineStep,
    TrainTokenizerPipelineStep,
)

pipeline_steps = [
    EnumerateCWEsPipelineStep,
    EnumerateTestcasesPipelineStep,
    PrintTestcasesPerCWEPipelineStep,
    FilterTestcasesPipelineStep,
    FilterCWEsPipelineStep,
    ValidateTestcaseFilesPipelineStep,
    ExtractExamplesPipelineStep,
    RemoveDuplicateExamplesPipelineStep,
    LabelExamplesPipelineStep,
    AssignSplitToExamplesPipelineStep,
    TrainTokenizerPipelineStep,
    TokenizeExamplesPipelineStep,
    TrainClassifierPipelineStep,
    PredictSplitPipelineStep,
    EvaluatePredictionsPipelineStep,
    PrintEvalulationTablePipelineStep,
]

__all__ = ["pipeline_steps"]
