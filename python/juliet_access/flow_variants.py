"""
    flow_variants: Description of Juliet flow variants, extracted from the v1.2 user guide.
"""

FLOW_VARIANT_DESCRIPTIONS = {
    flow_variant: {
        "FlowVariantID": flow_variant,
        "FlowType": flow_type,
        "Description": flow_variant_description,
    }
    for flow_variant, flow_type, flow_variant_description in [
        (1, "None", "Baseline – Simplest form of the flaw"),
        (2, "Control", "if(1) and if(0)"),
        (3, "Control", "if(5==5) and if(5!=5)"),
        (4, "Control", "if(STATIC_CONST_TRUE) and if(STATIC_CONST_FALSE)"),
        (5, "Control", "if(staticTrue) and if(staticFalse)"),
        (6, "Control", "if(STATIC_CONST_FIVE==5) and if(STATIC_CONST_FIVE!=5)"),
        (7, "Control", "if(staticFive==5) and if(staticFive!=5)"),
        (8, "Control", "if(staticReturnsTrue()) and if(staticReturnsFalse())"),
        (9, "Control", "if(GLOBAL_CONST_TRUE) and if(GLOBAL_CONST_FALSE)"),
        (10, "Control", "if(globalTrue) and if(globalFalse)"),
        (11, "Control", "if(globalReturnsTrue()) and if(globalReturnsFalse())"),
        (12, "Control", "if(globalReturnsTrueOrFalse())"),
        (13, "Control", "if(GLOBAL_CONST_FIVE==5) and if(GLOBAL_CONST_FIVE!=5)"),
        (14, "Control", "if(globalFive==5) and if(globalFive!=5)"),
        (15, "Control", "switch(6) and switch(7)"),
        (16, "Control", "while(1)"),
        (17, "Control", "for loops"),
        (18, "Control", "goto statements"),
        (
            21,
            "Control",
            "Flow controlled by value of a static global variable. All functions contained in one file.",
        ),
        (
            22,
            "Control",
            "Flow controlled by value of a global variable. Sink functions are in a separate file from sources.",
        ),
        (31, "Data", "Data flow using a copy of data within the same function"),
        (
            32,
            "Data",
            "Data flow using two pointers to the same value within the same function",
        ),
        (33, "Data", "Use of a C++ reference to data within the same function"),
        (
            34,
            "Data",
            "Use of a union containing two methods of accessing the same data (within the same function)",
        ),
        (
            41,
            "Data",
            "Data passed as an argument from one function to another in the same source file",
        ),
        (
            42,
            "Data",
            "Data returned from one function to another in the same source file",
        ),
        (
            43,
            "Data",
            "Data flows using a C++ reference from one function to another in the same source file",
        ),
        (
            44,
            "Control/Data",
            "Data passed as an argument from one function to a function in the same source file called via a function pointer",
        ),
        (
            45,
            "Data",
            "Data passed as a static global variable from one function to another in the same source file",
        ),
        (
            51,
            "Data",
            "Data passed as an argument from one function to another in different source files",
        ),
        (
            52,
            "Data",
            "Data passed as an argument from one function to another to another in three different source files",
        ),
        (
            53,
            "Data",
            "Data passed as an argument from one function through two others to a fourth; all four functions are in different source files",
        ),
        (
            54,
            "Data",
            "Data passed as an argument from one function through three others to a fifth; all five functions are in different source files",
        ),
        (
            61,
            "Data",
            "Data returned from one function to another in different source files",
        ),
        (
            62,
            "Data",
            "Data flows using a C++ reference from one function to another in different source files",
        ),
        (
            63,
            "Data",
            "Pointer to data passed from one function to another in different source files",
        ),
        (
            64,
            "Data",
            "void pointer to data passed from one function to another in different source files",
        ),
        (
            65,
            "Control/Data",
            "Data passed as an argument from one function to a function in adifferent source file called via a function pointer",
        ),
        (
            66,
            "Data",
            "Data passed in an array from one function to another in different source files",
        ),
        (
            67,
            "Data",
            "Data passed in a struct from one function to another in different source files",
        ),
        (
            68,
            "Data",
            "Data passed as a global variable in the “a” class from one function to another in different source files",
        ),
        (
            72,
            "Data",
            "Data passed in a vector from one function to another in different source files",
        ),
        (
            73,
            "Data",
            "Data passed in a linked list from one function to another in different source files",
        ),
        (
            74,
            "Data",
            "Data passed in a hash map from one function to another in different source files",
        ),
        (
            81,
            "Data",
            "Data passed in an argument to a virtual function called via a reference",
        ),
        (
            82,
            "Data",
            "Data passed in an argument to a virtual function called via a pointer",
        ),
        (
            83,
            "Data",
            "Data passed to a class constructor and destructor by declaring the class object on the stack",
        ),
        (
            84,
            "Data",
            "Data passed to a class constructor and destructor by declaring the class object on the heap and deleting it after use",
        ),
    ]
}
