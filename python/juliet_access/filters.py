"""
    filters: Functions that can be used with filter_testcases and filter_cwes in juliet_access.
"""

import pathlib


def testcase_has_object_file(testcase: dict) -> bool:
    """Selects testcases that have at least one object file associated with them."""
    files = testcase["Files"]
    for file in files:
        path: pathlib.Path = file["Path"]
        if path.name.endswith(".o"):
            return True

    return False


def testcase_has_cpp_file(testcase: dict) -> bool:
    """Selects testcases that have at least one c++ source file associated with them."""
    files = testcase["Files"]
    for file in files:
        path: pathlib.Path = file["Path"]
        if path.name.endswith(".cpp"):
            return True

    return False


def testcase_has_cwe(testcase: dict, allowed_cwes: list[int]) -> bool:
    """Selects testcases that are associated with one of the given cwes."""
    cwe: int = testcase["Weakness"]["WeaknessID"]
    return cwe in allowed_cwes


def testcase_has_flow_variant(testcase: dict, allowed_flow_variants: list[int]) -> bool:
    """Selects testcases that are associated with one of the given flow variants."""
    flow_variant: int = testcase["FlowVariant"]
    return flow_variant in allowed_flow_variants


def cwe_has_testcase(cwe: dict, testcases: list[dict]) -> bool:
    """Selects CWEs that have at least one testcase."""
    return (
        len(
            [
                testcase
                for testcase in testcases
                if testcase["Weakness"]["WeaknessID"] == cwe["WeaknessID"]
            ]
        )
        > 0
    )
