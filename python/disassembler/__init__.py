"""
    disassembler: Encapsulates logic for calling objdump and parsing its output.
"""

import logging
import pathlib
import re
import subprocess
import typing

import capstone

CS_DISASSEMBLER = capstone.Cs(capstone.CS_ARCH_X86, capstone.CS_MODE_64)
CS_DISASSEMBLER.syntax = capstone.CS_OPT_SYNTAX_INTEL
CS_DISASSEMBLER.detail = True


def parse_objdump_static_symbol_table(symbol_table_lines: list[str]) -> dict:
    """Parses the static symbol table output of objdump."""
    result = {}

    head = iter(symbol_table_lines)

    # Current scope: header
    assert next(head) == ""
    assert (matches := re.match(r"^(.*):\s+file format (.*)$", next(head))) is not None
    filename = matches[1]
    fileformat = matches[2]
    result["name"] = filename
    result["format"] = fileformat
    assert next(head) == ""

    # Current scope: symbol table
    assert next(head) == ("SYMBOL TABLE:")
    result["symbols"] = {}

    while True:
        try:
            symbol_line = next(head)
            if symbol_line == "":
                break
            table_entry_left, table_entry_right = symbol_line.split("\t")
            address = table_entry_left[:16]
            flags = set(table_entry_left[16:24]) - set([" "])
            section = table_entry_left[25:]
            size = table_entry_right[:16]
            name = table_entry_right[17:].strip()
            if name == ".group":
                continue

            if name.startswith(".hidden"):
                name = name[8:]

            if "@@" in name:
                name, lib = name.split("@@", maxsplit=1)
            else:
                lib = ""

            # (at least with c++ we cannot assert this)
            # assert name not in result["symbols"].keys()
            result["symbols"][name] = {
                "Address": address,
                "Flags": flags,
                "Section": section,
                "Libary": lib,
                "Size": size,
                "Name": name,
            }
        except StopIteration:
            break

    return result


def parse_objdump_dynamic_symbol_table(symbol_table_lines: list[str]) -> dict:
    """Parses the dynamic symbol table output of objdump."""
    result = {}

    head = iter(symbol_table_lines)

    # Current scope: header
    assert next(head) == ""
    assert (matches := re.match(r"^(.*):\s+file format (.*)$", next(head))) is not None
    filename = matches[1]
    fileformat = matches[2]
    result["name"] = filename
    result["format"] = fileformat
    assert next(head) == ""

    # Current scope: symbol table
    assert next(head) == ("DYNAMIC SYMBOL TABLE:")
    result["symbols"] = {}

    while True:
        try:
            symbol_line = next(head)
            if symbol_line == "":
                break
            table_entry_left, table_entry_right = symbol_line.split("\t")
            address = table_entry_left[:16]
            flags = set(table_entry_left[16:24]) - set([" "])
            section = table_entry_left[25:]
            size = table_entry_right[:16]
            lib_and_name = table_entry_right[18:].strip()
            if lib_and_name.count(" ") == 0:
                lib = ""
                name = lib_and_name
            elif lib_and_name.count(" ") >= 1:
                lib, name = lib_and_name.split(" ", maxsplit=1)

            if name == ".group":
                continue

            if name.startswith(".hidden"):
                name = name[8:]

            # (at least with c++ we cannot assert this)
            # assert name not in result["symbols"].keys()
            result["symbols"][name] = {
                "Address": address,
                "Flags": flags,
                "Section": section,
                "Library": lib,
                "Size": size,
                "Name": name,
            }
        except StopIteration:
            break

    return result


def parse_objdump_disassembly(disassembly_lines: list[str]) -> dict:
    """Takes the standard output of objdump and returns it in a structured way."""
    result = {}

    head = iter(disassembly_lines)

    # Current scope: header
    assert next(head) == ""
    assert (matches := re.match(r"^(.*):\s+file format (.*)$", next(head))) is not None
    filename = matches[1]
    fileformat = matches[2]
    result["name"] = filename
    result["format"] = fileformat
    assert next(head) == ""
    assert next(head) == ""

    current_section = None
    result["sections"] = {}
    while True:
        # Current scope: outer
        try:
            outer_line = next(head)
            if (
                matches := re.match(r"^Disassembly of section (.+):$", outer_line)
            ) is not None:
                current_section = matches[1]
                result["sections"][current_section] = {}
                result["sections"][current_section]["functions"] = {}
                assert next(head) == ""

            elif (
                matches := re.match(r"[0-9a-f]+\s<(([^@]+)(@(.+))?)>:", outer_line)
            ) is not None:
                assert current_section is not None
                current_function = matches[1]
                function_name = matches[2]
                function_ref = matches[4]

                # Current scope: in function
                instructions = objdump_parse_instructions(head)

                result["sections"][current_section]["functions"][current_function] = {
                    "instructions": instructions,
                    "name": function_name,
                    "ref": function_ref,
                }
            else:
                raise ValueError(
                    f"Unexpected line {outer_line}, expecting start of section or function"
                )
        except StopIteration:
            break

    return result


def objdump_parse_instructions(head: typing.Iterator[str]):
    """Parses instructions from the given iterator.

    We also request a second disassembly by capstone."""
    instructions = []

    # Run file parser
    try:
        while (inner_line := next(head)) != "":
            components = inner_line.split("\t")
            assert len(components) in (2, 3)

            # First component: Address
            assert (matches := re.match(r"\s+([0-9a-f]+):", components[0])) is not None
            address = matches[1]

            # Second component: Binary representation
            assert (
                matches := re.findall(r"([0-9a-f][0-9a-f])\s", components[1])
            ) is not None
            binary = [str(match) for match in matches]

            # Third component: Instruction and comments
            if len(components) == 3:
                assert (
                    matches := re.match(
                        r"((rep\s|repnz\s|repz\s|lock\s)?[a-z0-9]+)(\s+([^#]+))?(#\s*(.*))?\s?$",
                        components[2],
                    )
                ) is not None
                operation = matches[1]
                operand = matches[4].strip() if matches[4] is not None else None
                comment = matches[6]

                instruction = {
                    "address": address,
                    "od_operation": operation,
                    "od_operand": operand,
                    "od_comment": comment,
                    "binary": binary,
                }

                instructions += [instruction]
            elif len(components) == 2:
                # We can assume that instruction already exists if a 2-tab line is encountered
                instruction["binary"] += binary

    except StopIteration:
        pass

    augment_instructions_with_capstone(instructions)

    return instructions


def augment_instructions_with_capstone(instructions):
    """This function uses the hex representation by objdump to get a second opinion from capstone."""
    for instruction in instructions:
        binary = bytes.fromhex(" ".join(instruction["binary"]))
        if (
            len(
                disassembly := list(
                    CS_DISASSEMBLER.disasm(
                        binary, offset=int(instruction["address"], 16)
                    )
                )
            )
            == 1
        ):
            disassembly = disassembly[0]
            instruction["cs_operation"] = disassembly.mnemonic
            instruction["cs_operand"] = (
                disassembly.op_str if len(disassembly.op_str) > 0 else None
            )

            if (
                instruction["cs_operation"] != instruction["od_operation"]
            ):  # Little sanity check
                if instruction["cs_operation"] not in [
                    "nop",
                    "rep movsb",
                    "wait",
                    "rep movsd",
                    "rep movsq",
                    "rep stosb",
                    "rep stosd",
                    "repne scasb",
                    "ret",
                    "repe cmpsb",
                    "rep stosq",
                    "movsb",
                    "movsq",
                ]:
                    print(
                        f"Mismatch: {instruction['cs_operation']} <-> {instruction['od_operation']}"
                    )
        else:
            print(f"Having trouble disassembling {instruction}, using objdump")
            instruction["cs_operation"] = instruction["od_operation"]
            instruction["cs_operand"] = instruction["od_operand"]


def disassemble_elf(elf_files: list[pathlib.Path], logger: logging.Logger) -> list[str]:
    """Disassembles a file in ELF format using objdump and returns a parsed representation."""

    # (1) Ensure that the files are linked
    first_elf_file = list(sorted(elf_files, key=lambda elf_file: elf_file.name))[0]

    linked_elf_file: pathlib.Path = (
        first_elf_file.parent / f"linked-{first_elf_file.name}"
    )

    if not linked_elf_file.exists() or linked_elf_file.stat().st_size == 0:
        support_objects_path = first_elf_file.parent.parent.parent / "testcasesupport"
        if not support_objects_path.exists():
            support_objects_path = (
                first_elf_file.parent.parent.parent.parent / "testcasesupport"
            )

        support_object_files = [support_objects_path / "io.c.o"]
        link_command = f"g++ -Wl,--unresolved-symbols=ignore-in-object-files -no-pie -o {str(linked_elf_file)} {' '.join([str(elf_file) for elf_file in elf_files + support_object_files])}"
        try:
            subprocess.run(
                link_command, shell=True, text=True, check=True, capture_output=True
            )
            logger.debug(f"Linking of {str(linked_elf_file)} successful!")
        except subprocess.CalledProcessError as cpe:
            logger.fatal(
                f"Linking {str(linked_elf_file)} failed. Return code {cpe.returncode}"
            )
            logger.fatal(f"  Command line: {link_command}")
            logger.fatal(f"  Standard output: {cpe.output}")
            logger.fatal(f"  Standard error: {cpe.stderr}")
            raise cpe

    # (2) Call objdump to obtain disassembly, static and dynamic symbol tables
    disassembly_command = (
        f"objdump -d -C -Mintel -Mintel-mnemonic -Mintel64 {str(linked_elf_file)}"
    )
    static_symbol_table_command = f"objdump -C -t {str(linked_elf_file)}"
    dynamic_symbol_table_command = f"objdump -C -T {str(linked_elf_file)}"

    import os

    target_env = os.environ.copy()
    target_env["LC_ALL"] = "C"
    target_env["LC_MESSAGES"] = "C"
    target_env["LC_CTYPE"] = "C"

    objdump_output_disassembly = subprocess.run(
        disassembly_command,
        shell=True,
        capture_output=True,
        text=True,
        check=True,
        env=target_env,
    )
    objdump_output_static_symbol_table = subprocess.run(
        static_symbol_table_command,
        shell=True,
        capture_output=True,
        text=True,
        check=True,
        env=target_env,
    )
    objdump_output_dynamic_symbol_table = subprocess.run(
        dynamic_symbol_table_command,
        shell=True,
        capture_output=True,
        text=True,
        check=True,
        env=target_env,
    )

    # (3) Parse disassembly and symbol tables
    disassembly = parse_objdump_disassembly(
        objdump_output_disassembly.stdout.splitlines()
    )
    static_symbol_table = parse_objdump_static_symbol_table(
        objdump_output_static_symbol_table.stdout.splitlines()
    )
    dynamic_symbol_table = parse_objdump_dynamic_symbol_table(
        objdump_output_dynamic_symbol_table.stdout.splitlines()
    )

    # (4) Sanity check
    assert disassembly["name"] == static_symbol_table["name"]
    assert disassembly["format"] == static_symbol_table["format"]

    combined_symbol_table = dict(static_symbol_table["symbols"])
    combined_symbol_table.update(dynamic_symbol_table["symbols"])

    # (5) Collect and merge results
    result = {
        "symbols": combined_symbol_table,
        "sections": disassembly["sections"],
        "name": disassembly["name"],
        "format": disassembly["format"],
    }

    return result


__all__ = ["disassemble_elf"]
